---
- name: Unregister from RHN if reachable - {{ instance_name }}
  ansible.builtin.command:
    cmd: 'ssh -o "StrictHostKeyChecking=off" ec2-user@{{ instance_name }}.{{ dns_suffix }}.ansible-labs.de "sudo subscription-manager unregister"'
  when:
    - instance_name is defined
    - dns_suffix is defined
  ignore_errors: true

- name: Remove DNS record
  ansible.builtin.include_role:
    name: nsupdate
  vars:
    shortname: "{{ instance_name }}"
    mode: remove
  when:
    - dns_update | bool

- name: Gather VPC information
  amazon.aws.ec2_vpc_net_info:
    filters:
      "tag:Name": "{{ ec2_vpc_name }}"
    region: "{{ ec2_region }}"
  register: vpc

- name: Gather subnet information
  amazon.aws.ec2_vpc_subnet_info:
    filters:
      "tag:Name": "{{ ec2_vpc_name }}"
    region: "{{ ec2_region }}"
  register: subnet

- name: Remove EC2 instance - {{ instance_name }}
  amazon.aws.ec2_instance:
    name: "{{ instance_name }}"
    region: "{{ ec2_region }}"
    vpc_subnet_id: "{{ subnet.subnets[0].subnet_id }}"
    state: absent
  when:
    - subnet.subnets[0].subnet_id is defined

- name: Get instances for this VPC
  amazon.aws.ec2_instance_info:
    filters:
      "vpc-id": "{{ vpc.vpcs[0].vpc_id }}"
    region: "{{ ec2_region }}"
  register: vpc_instance_list
  when:
    - vpc.vpcs != []

- name: Initialize variable _vpc_empty
  ansible.builtin.set_fact:
    _vpc_empty: false

- name: Check if VPC does not exist or there are no instances
  ansible.builtin.set_fact:
    _vpc_empty: true
  when: vpc.vpcs == [] or vpc_instance_list.instances == [] | default('[]')

- name: Remove SSH Key pair
  amazon.aws.ec2_key:
    name: "{{ ec2_vpc_name }}"
    state: absent
    region: "{{ ec2_region }}"
  when:
    - _vpc_empty | bool

- name: Remove EC2 security group - {{ instance_name }}
  amazon.aws.ec2_security_group:
    name: "{{ instance_name }}"
    region: "{{ ec2_region }}"
    state: absent

- name: Remove Subnet
  amazon.aws.ec2_vpc_subnet:
    vpc_id: "{{ subnet.subnets[0].vpc_id }}"
    state: absent
    cidr: "172.31.1.0/24"
    region: "{{ ec2_region }}"
  when:
    - subnet.subnets[0].subnet_id is defined
    - _vpc_empty | bool

- name: Gather Route Table information
  amazon.aws.ec2_vpc_route_table_info:
    filters:
      "tag:Name": "{{ ec2_vpc_name }}"
    region: "{{ ec2_region }}"
  register: route_table

- name: Remove Route Table
  amazon.aws.ec2_vpc_route_table:
    route_table_id: "{{ route_table.route_tables[0].id }}"
    vpc_id: "{{ vpc.vpcs[0].vpc_id }}"
    state: absent
    region: "{{ ec2_region }}"
    lookup: id
  when:
    - route_table.route_tables[0].id is defined
    - _vpc_empty | bool

- name: Gather internet gateway information
  amazon.aws.ec2_vpc_igw_info:
    filters:
      "tag:Name": "{{ ec2_vpc_name }}"
    region: "{{ ec2_region }}"
  register: igw

- name: Remove internet gateway
  amazon.aws.ec2_vpc_igw:
    state: absent
    vpc_id: "{{ vpc.vpcs[0].vpc_id }}"
    region: "{{ ec2_region }}"
  when:
    - igw.internet_gateways[0].internet_gateway_id is defined
    - _vpc_empty | bool

- name: Remove empty VPC {{ ec2_vpc_name }}
  amazon.aws.ec2_vpc_net:
    name: "{{ ec2_vpc_name }}"
    cidr_block: "172.31.0.0/16"
    state: absent
    region: "{{ ec2_region }}"
  when:
    - _vpc_empty | bool
